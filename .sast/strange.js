const fs = require("fs")

console.log("=== Strange Analyzer ===")

let start = new Date()

let vulnerability = process.env.PATTERN
let severity = process.env.SEVERITY
let confidence = process.env.CONFIDENCE
let results = process.env.RESULTS

let scannerName = process.env.SCANNER_NAME
let scannerId = process.env.SCANNER_ID
let scannerSrc = process.env.SCANNER_SRC
let scannerVendor = process.env.SCANNER_VENDOR
let scannerVersion = process.env.SCANNER_VERSION

/*
console.log("vulnerability", vulnerability)
console.log("severity", severity)
console.log("confidence", confidence)
console.log("results", results)
*/

// ========== Helpers ==========
let btoa = (string) => Buffer.from(string).toString("base64")

let get_cve_id = (description, path, line) => `${btoa(description)}-${btoa(path)}-${btoa(line)}`

let get_scanner = () => {
  return {
    id: scannerId, name: scannerName
  }
}

let get_class = () => { return "" } 

let get_method = () => { return "" } 

let get_dependency = () => { 
  return { 
    package: {} 
  } 
}        

let get_an_identifier = () => {
  let type = () => `${scannerName}_rule_id`
  //let type = () => Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) 
  let name = () => `${scannerName} rule ID security/detect-${vulnerability}`
  //let name = () => Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) 
  let value = () => `security/detect-${vulnerability}`
  //let value = () => Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) 
  let url = () => scannerSrc

  return {
    type: type(), name: name(), value: value(), url: url()
  }
}
// ========== End of Helpers ==========

// ========== Generate SAST report ==========
let vulnerabilities = process.env.RESULTS  
      .split("\n")
      .map(item => item.split(":"))
      .map(item => {
      return {
        category: "sast",
        name: `🔴: ${vulnerability}`,
        message: `📝: ${item[2].trim()}`,
        descrition: `🖐️: ${item[2].trim()}`,
        cve: get_cve_id(item[2], item[0], item[1]),
        severity: severity,
        confidence: confidence,
        scanner: get_scanner(),
        location: {
          file: item[0], 
          start_line: parseInt(item[1],10),
          end_line: parseInt(item[1],10),
          class: get_class(),
          method: get_method(),
          dependency: get_dependency()
        },
        identifiers: [
          get_an_identifier()
        ]                       
      } // return
    })

//console.log(JSON.stringify(vulnerabilities, null, 2)) // prettify
//fs.writeFileSync("./vulnerabilities-report.json", JSON.stringify(vulnerabilities, null, 2))

let end = new Date() - start

let sastReport = {
  version: "3.0",
  vulnerabilities: vulnerabilities,
  remediations: [],
  scan: {
    scanner: {
      id: scannerId,
      name: scannerName,
      url: scannerSrc,
      vendor: {
        name: scannerVendor
      },
      version: scannerVersion
    },
    type: "sast",
    start_time: start,
    end_time: end,
    status: "success" // TODO try catch
  }
}

fs.writeFileSync("./gl-sast-report.json", JSON.stringify(sastReport, null, 2))






